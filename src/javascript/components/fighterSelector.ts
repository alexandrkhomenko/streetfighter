import { createElement } from '../helpers/domHelper';
import { renderArena } from './arena';
// @ts-ignore
import versusImg from '../../../resources/versus.png';
import { createFighterPreview } from './fighterPreview';
import { fighterService } from '../services/fighterService';
import {Fighter} from "../../../resources/type/type";


export function createFightersSelector() {
    let selectedFighters: Fighter[] = [];

    return async (event: any, fighterId: string) => {
        const fighter = await getFighterInfo(fighterId);
        const [playerOne, playerTwo] = selectedFighters;
        const firstFighter: Fighter = playerOne != null ? playerOne : fighter;
        const secondFighter: Fighter = Boolean(playerOne) ? playerTwo != null ? playerTwo : fighter : playerTwo;
        selectedFighters = [firstFighter, secondFighter];

        renderSelectedFighters(selectedFighters);
    };
}

const fighterDetailsMap = new Map();


export async function getFighterInfo(fighterId: string) {
    if (!fighterDetailsMap.has(fighterId)) {
        fighterDetailsMap.set(fighterId, await fighterService.getFighterDetails(fighterId))
    }

    return fighterDetailsMap.get(fighterId)
}

function renderSelectedFighters(selectedFighters: Fighter[]) {
    const fightersPreview = document.querySelector('.preview-container___root');
    const [playerOne, playerTwo] = selectedFighters;
    const firstPreview = createFighterPreview(playerOne, 'left');
    const secondPreview = createFighterPreview(playerTwo, 'right');
    const versusBlock = createVersusBlock(selectedFighters);

    fightersPreview.innerHTML = '';
    fightersPreview.append(firstPreview, versusBlock, secondPreview);
}

function createVersusBlock(selectedFighters: Fighter[]) {
    const canStartFight = selectedFighters.filter(Boolean).length === 2;
    const onClick = () => startFight(selectedFighters);
    const container = createElement({ tagName: 'div', className: 'preview-container___versus-block' });
    const image = createElement({
        tagName: 'img',
        className: 'preview-container___versus-img',
        attributes: { src: versusImg },
    });
    const disabledBtn = canStartFight ? '' : 'disabled';
    const fightBtn = createElement({
        tagName: 'button',
        className: `preview-container___fight-btn ${disabledBtn}`,
    });

    fightBtn.addEventListener('click', onClick, false);
    fightBtn.innerText = 'Fight';
    container.append(image, fightBtn);

    return container;
}

function startFight(selectedFighters: Fighter[]) {
    renderArena(selectedFighters);
}
