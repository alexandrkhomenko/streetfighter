import { createElement } from '../helpers/domHelper';
import { createFightersSelector } from './fighterSelector';
import {Fighter, Attributes} from "../../../resources/type/type";

export function createFighters(fighters: any){
    const selectFighter = createFightersSelector();
    const container = createElement({ tagName: 'div', className: 'fighters___root' });
    const preview = createElement({ tagName: 'div', className: 'preview-container___root' });
    const fightersList = createElement({ tagName: 'div', className: 'fighters___list' });
    const fighterElements = fighters.map((fighter: Fighter) => createFighter(fighter, selectFighter));

    fightersList.append(...fighterElements);
    container.append(preview, fightersList);

    return container;
}

function createFighter(fighter: Fighter, selectFighter: any): HTMLElement {
    const fighterElement = createElement({ tagName: 'div', className: 'fighters___fighter' });
    const imageElement = createImage(fighter);
    const onClick = (event: MouseEvent) => selectFighter(event, fighter._id);

    fighterElement.append(imageElement);
    fighterElement.addEventListener('click', onClick, false);

    return fighterElement;
}

function createImage(fighter: Fighter): HTMLElement {
    const { source, name } = fighter;
    const attributes: Attributes = {
        src: source,
        title: name,
        alt: name,
    };
    const imgElement = createElement({
        tagName: 'img',
        className: 'fighter___fighter-image',
        attributes
    });

    return imgElement;
}
