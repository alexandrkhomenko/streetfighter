import{showModal} from "./modal"
import { createElement } from '../../helpers/domHelper';
import App from '../../app'

export function showWinnerModal(fighter) {
  const attributes = { src: fighter.source };

  const bodyElement = createElement({
    tagName: 'img',
    className: `fighter-preview___${fighter.position}`,
    title: fighter.name,
    alt: fighter.name,
    attributes
  });

  showModal({
    title: `${fighter.name} Winner `,
    bodyElement: bodyElement,
    onClose: () => {
      const root = document.getElementById('root');
      root.innerHTML = '';
      new App();
    }
  });
}
