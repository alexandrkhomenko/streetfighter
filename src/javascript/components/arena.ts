import { createElement } from '../helpers/domHelper';
import { createFighterImage } from './fighterPreview';
import {showWinnerModal} from "./modal/winner"
import {Fighter} from "../../../resources/type/type";

import {fight} from './fight';



export async function renderArena(selectedFighters: Fighter[]) {
    const root: HTMLElement = document.getElementById('root');
    const arena = createArena(selectedFighters);

    root.innerHTML = '';
    root.append(arena);

    await fight(selectedFighters[0], selectedFighters[1])
        .then(res => showWinnerModal(res));
}

function createArena(selectedFighters: Fighter[]): HTMLElement {
    const arena: HTMLElement = createElement({ tagName: 'div', className: 'arena___root' });
    const healthIndicators = createHealthIndicators(selectedFighters[0], selectedFighters[1]);
    const fighters = createFighters(selectedFighters[0], selectedFighters[1]);

    arena.append(healthIndicators, fighters);
    return arena;
}

function createHealthIndicators(leftFighter: Fighter, rightFighter: Fighter) {
    const healthIndicators: HTMLElement = createElement({ tagName: 'div', className: 'arena___fight-status' });
    const versusSign: HTMLElement = createElement({ tagName: 'div', className: 'arena___versus-sign' });
    const leftFighterIndicator = createHealthIndicator(leftFighter, 'left');
    const rightFighterIndicator = createHealthIndicator(rightFighter, 'right');

    healthIndicators.append(leftFighterIndicator, versusSign, rightFighterIndicator);
    return healthIndicators;
}

function createHealthIndicator(fighter: Fighter, position: string): HTMLElement {
    const { name } = fighter;
    const container: HTMLElement = createElement({ tagName: 'div', className: 'arena___fighter-indicator' });
    const fighterName: HTMLElement = createElement({ tagName: 'span', className: 'arena___fighter-name' });
    const indicator: HTMLElement = createElement({ tagName: 'div', className: 'arena___health-indicator' });
    const bar: HTMLElement = createElement({ tagName: 'div', className: 'arena___health-bar', attributes: { id: `${position}-fighter-indicator` }});

    fighterName.innerText = name;
    indicator.append(bar);
    container.append(fighterName, indicator);

    return container;
}

function createFighters(firstFighter: Fighter, secondFighter: Fighter): HTMLElement {
    const battleField: HTMLElement = createElement({ tagName: 'div', className: `arena___battlefield` });
    const firstFighterElement: HTMLElement = createFighter(firstFighter, 'left');
    const secondFighterElement: HTMLElement = createFighter(secondFighter, 'right');

    battleField.append(firstFighterElement, secondFighterElement);
    return battleField;
}

function createFighter(fighter: Fighter, position: string): HTMLElement {
    const imgElement = createFighterImage(fighter);
    const positionClassName = position === 'right' ? 'arena___right-fighter' : 'arena___left-fighter';
    const fighterElement = createElement({
        tagName: 'div',
        className: `arena___fighter ${positionClassName}`,
    });

    fighterElement.append(imgElement);
    return fighterElement;
}
