import { controls } from '../../constants/controls';
import {Fighter} from "../../../resources/type/type";


export async function fight(firstFighter: Fighter, secondFighter: Fighter) {

    let KeySet = new Set();

    firstFighter.Block = false;
    firstFighter.HeavyKicks = true;
    firstFighter.position = 'left';

    secondFighter.Block = false;
    secondFighter.HeavyKicks = true;
    secondFighter.position = 'right';


    return new Promise((resolve) => {

        let leftCount = firstFighter.health;
        let rightCount = secondFighter.health;

        function counterRight(damage: number): number {
            return rightCount = rightCount - damage;
        }

        function counterLeft(damage: number): number {
            return leftCount = leftCount - damage;
        }

        function Timer(fighter: Fighter) {
            let arr: any = [];

            function HeavyKickTimer() {
                let TimerId = setTimeout(() => {
                    fighter.HeavyKicks = true;
                    for (let item of arr) {
                        clearTimeout(item);

                    }
                    arr = [];
                }, 10000);
                arr.push(TimerId);
            }

            HeavyKickTimer();
        }


        async function MediumKick(event: KeyboardEvent) {

            KeySet.add(event.code);

            if (event.code === controls.PlayerOneAttack && firstFighter.Block === false) {
                const damage = await getDamage(firstFighter, secondFighter);
                AddHealth(damage, secondFighter, firstFighter);
            }
            if (event.code === controls.PlayerTwoAttack && secondFighter.Block === false) {
                const damage = await getDamage(secondFighter, firstFighter);
                AddHealth(damage, firstFighter, secondFighter);
            }
        }

        function BlockDown(event: KeyboardEvent) {
            if (event.code === controls.PlayerOneBlock) {
                firstFighter.Block = true;
            }
            if (event.code === controls.PlayerTwoBlock) {
                secondFighter.Block = true;
            }
        }

        function BlockUp(event: KeyboardEvent) {

            KeySet.delete(event.code);


            if (event.code === controls.PlayerOneBlock) {
                firstFighter.Block = false;
            }
            if (event.code === controls.PlayerTwoBlock) {
                secondFighter.Block = false;
            }
        }


        function HeavyKick(combination: string[], attacker: Fighter, defender: Fighter){
            for (let code of combination) {
                let ArrInclude = combination.filter(item => KeySet.has(item));
                if(ArrInclude.length === 3 && attacker.HeavyKicks === true){
                    const damage = getHitPower(attacker) * 2;
                    AddHealth(damage, defender, attacker);
                    attacker.HeavyKicks = false;
                    Timer(attacker);
                }
            }
        }


        function KeydownEventFunction(event: KeyboardEvent){
            MediumKick(event);
            BlockDown(event);
            HeavyKick(controls.PlayerOneCriticalHitCombination, firstFighter, secondFighter);
            HeavyKick(controls.PlayerTwoCriticalHitCombination, secondFighter, firstFighter)
        }


        document.addEventListener(
            'keydown',
            KeydownEventFunction,
            false);

        document.addEventListener(
            'keyup',
            BlockUp,
            false);


        function AddHealth(damage: number, defender: Fighter, attacker: Fighter) {
            let counter: number = defender.position === 'left'
                ? counterLeft(damage)
                : counterRight(damage);

            const Indicator: HTMLElement = document.getElementById(`${defender.position}-fighter-indicator`);

            let widthIndicator: number = (100 * counter) / defender.health;

            if (widthIndicator <= 0) {
                Indicator.style.width = `0px`;
                document.removeEventListener(
                    'keydown',
                    KeydownEventFunction,
                    false );
                document.removeEventListener(
                    'keyup',
                    BlockUp,
                    false);
                resolve(attacker);
            }

            Indicator.style.width = `${widthIndicator}%`
        }
    });

}


export function getDamage(attacker: Fighter, defender: Fighter): number {

    if(defender.Block === true){
        return 0
    }
    if (attacker.Block === true){
        return 0
    }
    if (defender.Block === false) {
        const hitPower = getHitPower(attacker);
        const blockPower = getBlockPower(defender);

        if (blockPower >= hitPower) {
            return 0;
        }

        return hitPower - blockPower;
    }
}

export function getHitPower(fighter: Fighter): number {
    const criticalHitChance: number = Math.random() + 1;
    return  fighter.attack * criticalHitChance
}

export function getBlockPower(fighter: Fighter): number {
    const dodgeChance: number = Math.random() + 1;
    return fighter.defense * dodgeChance;
}

