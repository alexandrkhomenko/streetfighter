import { createElement } from '../helpers/domHelper';
import {Fighter, Attributes} from "../../../resources/type/type";


export function createFighterPreview(fighter: Fighter, position: string): HTMLElement {
    const { source, name, health, attack, defense } = fighter;
    const attributes: Attributes = {
        src: source,
        title: name,
        alt: name,};

    const positionClassName: string = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
    const fighterElement = createElement({
        tagName: 'div',
        className: `fighter-preview___root ${positionClassName}`,
    });


    const imgElement = createElement({
        tagName: 'img',
        className: `fighter-preview___${position}`,
        attributes
    });


    const fighterName = createElement({
        tagName: 'span',
        className: 'fighter-preview___fighter-name'
    });
    fighterName.innerHTML = name;


    const attackSkill = createElement({
        tagName: 'span',
        className: 'fighter-preview___fighter-skill'
    });
    attackSkill.innerHTML = `attack ${attack}`;


    const healthSkill = createElement({
        tagName: 'span',
        className: 'fighter-preview___fighter-skill'
    });
    healthSkill.innerHTML = `health ${health}`;


    const defenseSkill = createElement({
        tagName: 'span',
        className: 'fighter-preview___fighter-skill'
    });
    defenseSkill.innerHTML = `defense ${defense}`;


    function createSkillIndicator(SkillName: string, skill: number): HTMLElement {
        const indicator = createElement({ tagName: 'div', className: 'arena___health-indicator' });
        indicator.append(SkillName);
        indicator.style.width = '90%';
        indicator.style.background = `linear-gradient(90deg, 
          rgba(225,65,21,1) 0%, 
          rgba(227,19,65,0.958420868347339) ${skill * 20}%, 
          rgba(0,255,232,0.2310832162921348) ${skill * 20}%)`;
        return indicator;
    }


    const indicatorAttack = createSkillIndicator(attackSkill, attack);
    const indicatorDefense = createSkillIndicator(defenseSkill, defense);
    const indicatorHealth = createSkillIndicator(healthSkill, health);
    indicatorHealth.style.background = `linear-gradient(90deg, 
          rgba(225,65,21,1) 0%, 
          rgba(227,19,65,0.958420868347339) ${(100 * health) / 60}%, 
          rgba(0,255,232,0.2310832162921348) ${(100 * health) / 60}%)`;


    const skill = createElement({
        tagName: 'div',
        className: `fighter-preview___skills`,
    });
    skill.append( fighterName, indicatorHealth, indicatorAttack, indicatorDefense);


    fighterElement.append(skill, imgElement);
    return fighterElement;
}



export function createFighterImage(fighter: Fighter): HTMLElement {
    const { source, name } = fighter;
    const attributes: Attributes = {
        src: source,
        title: name,
        alt: name
    };
    const imgElement = createElement({
        tagName: 'img',
        className: 'fighter-preview___img',
        attributes,
    });

    return imgElement;
}
