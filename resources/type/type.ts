
type Fighter = {
    attack: number,
    defense: number,
    health: number,
    name: string,
    source: string,
    _id: string,
    Block?: boolean,
    HeavyKicks?: boolean,
    position?: string
}

type Attributes = {
    src: string,
    title: string,
    alt: string,

};



export {Fighter, Attributes};
